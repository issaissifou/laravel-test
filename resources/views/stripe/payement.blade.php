@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Payement') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('payemnt_store') }}" id="form">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        
                        <input type="hidden" name="payement_method" id="payement_method" {{ old('payement_method') ? 'checked' : '' }}>

                        <!-- Stripe Elements Placeholder -->
                        <div id="card-element"></div>
                        <div class="form-group row mt-4">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary btn-block" id="sublit-button">
                                    {{ __('Valider') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('extra-js')
<script src="https://js.stripe.com/v3"></script>
<script>
    const stripe = Stripe("{{env('STRIPE_KEY')}}");

    const elements = stripe.elements();
    const cardElement = elements.create('card', {
        classes: {
            base: 'StripeElement stripe'
        }
    });
    
        cardElement.mount('#card-element');

        const cardButton = document.getElementById('submit-button');

        cardButton.addEvenListener('click', async(e) => {
            e.preventDefault();

            const { payementMethod, error } = await stripe.createPaymentMethod('card', cardElement);

            if (error) {
                alert(error)
            } else {
                document.getElementById('payement_method').value = payementMethod.id
            }

            document.getElementById('form').submit();

        });
</script>
<script src="{{ asset('js/test.js') }}"></script>
@endsection


