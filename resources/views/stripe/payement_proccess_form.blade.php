@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="alert alert-danger" role="alert" id="alert" style="display: none">
            </div>
            <div class="card">
                
                <div class="card-header">{{ __('Payement') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('payement_proccess') }}" id="stripe">
                        <input id="card-holder-name" type="text" />
                        {{-- @csrf --}}
                        <!-- Stripe Elements Placeholder -->
                        <div id="card-element"></div>
                        <input name="pmethod" id="pmethod" type="hidden" value="" />

                        <!-- Stripe Elements Placeholder -->
                        <div class="form-group row mt-4">
                            <div class="col-md-6 offset-md-4">
                                <button id="card-button">
                                    {{ __('Process Payment') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('extra-js')
<script src="https://js.stripe.com/v3"></script>

<script>

    const alert = document.getElementById('alert')
    const stripe = Stripe("{{env('STRIPE_KEY')}}");

    const elements = stripe.elements();
    const cardElement = elements.create('card', {
        classes: {
            base: 'StripeElement stripe'
        }
    });
    
        cardElement.mount('#card-element');

        const cardHolderName = document.getElementById('card-holder-name');
        const form = document.getElementById('stripe');


        form.addEventListener('submit', async(e) => {
            e.preventDefault();
            
           console.log(form)
            const { paymentMethod, error } = await stripe.createPaymentMethod(
                'card', cardElement, {
                    billing_details: { name: cardHolderName.value }
                }
            );

            if (error) {
                // Display "error.message" to the user...
                alert.textContent = error.message;
                alert.style.display = "block"
            } else {
                document.getElementById('card-button').disabled = true
                console.log('Card verified successfully');
                console.log(paymentMethod.id);
                document.getElementById('pmethod').setAttribute('value', paymentMethod.id);
                form.submit();
            }
        });

        // -------------------------------

</script>
{{-- <script src="{{ asset('js/test.js') }}"></script> --}}
@endsection


