<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/payement', [App\Http\Controllers\PayementController::class, 'payement'])->name('payement')->middleware('auth');
Route::post('/payemnt_store', [App\Http\Controllers\PayementController::class, 'payemnt_store'])->name('payemnt_store')->middleware('auth');

Route::get('/payement_proccess_form', [App\Http\Controllers\PayementController::class, 'payement_proccess_form'])->name('payement_proccess_form')->middleware('auth');
Route::post('/payement_proccess', [App\Http\Controllers\PayementController::class, 'payement_proccess'])->name('payement_proccess')->middleware('auth');
