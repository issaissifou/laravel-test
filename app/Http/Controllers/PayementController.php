<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Laravel\Cashier\Exceptions\IncompletePayment;

class PayementController extends Controller
{
    public function index()
    {

    }

    public function payement()
    {
        $test = "testttt";
        return view('stripe.payement', compact('test'));
    }

    public function payemnt_store(Request $request)
    {
        $authed_user = auth()->user();
        $amount = 5000;

        $authed_user->charge($amount, $request->payemnt_method);
    }

    public function payement_proccess_form()
    {
        return view('stripe.payement_proccess_form');
    }

    public function payement_proccess(Request $request)
    {
        
        $user = User::where('id', 1)->first();

        try {
            $stripeCharge =  $user->charge(100, $request->pmethod);
        } catch (IncompletePayment $exception) {
            return redirect()->route(
                'cashier.payment',
                [$exception->payement->id, 'redirect' => route('payment')]
            );
        }

        dd($stripeCharge);
    } 
}
